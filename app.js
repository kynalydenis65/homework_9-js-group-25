
function createListFromArray(array, parent = document.body) {
    const list = document.createElement('ul');

    const items = array.map(item => {
        const listItem = document.createElement('li');
        listItem.textContent = item;
        return listItem;
    });

    list.append(...items);

    parent.appendChild(list);
}

const dataArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
createListFromArray(dataArray);
